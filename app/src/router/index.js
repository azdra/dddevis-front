import { createRouter, createWebHistory } from 'vue-router'
import ListeProjets from '../views/ListeProjets.vue'
import ListeFactures from '../views/ListeFactures.vue'
import Projet from '../views/Projet.vue'
import Facture from '../views/Facture.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: ListeProjets
    },
    {
      path: '/factures',
      name: 'factures',
      component: ListeFactures
    },
    {
      path: '/projet/:id',
      name: 'projet',
      component: Projet
    },
    {
      path: '/facture/:id',
      name: 'facture',
      component: Facture
    },
  ]
})

export default router
