import axios from 'axios'

const address = {

}

const client = {
    name: "",
    firstName: "",
    socialreason: "ESGI",
    mail: "esgi@myges.fr",
    phone: "0600000000",
    address: address
}  

const lines = [
    {
        id: 1,
        description: "Super ligne",
        unitPriceNoTax: 158.98,
        quantity: 1,
        totalNoTax: 158.98,
        tax: 0.1,
        total: 174.878
    },
    {
        id: 2,
        description: "2eme ligne",
        unitPriceNoTax: 100,
        quantity: 2,
        totalNoTax: 200,
        tax: 0.1,
        total: 210
    }
]

const Projets = [
    {
        id: 1, 
        comment: "Super Projet", 
        totalNoTax: 158.98, 
        totalTax: 15.898, 
        total: 174.878,
        date: new Date(),
        client: {
            name: "",
            firstName: "",
            socialreason: "ESGI",
            mail: "esgi@myges.fr",
            phone: "0600000000"
        },
        lines: lines
    }
];

const Factures = [
    {
        id: 1, 
        comment: "Super Facture", 
        totalNoTax: 158.98, 
        totalTax: 15.898, 
        total: 174.878,
        date: new Date(),
        client: client,
        lines: lines
    }
];
export const makeRequest = async (url, method, body={}) => {
    const response = await axios({
      url: `${url}`,
      method,
      data: body
    })
    return response.data
  }
  

export const getAllProjets = ()=>{
    return makeRequest("http://localhost:8080/quote", 'GET')
}

export const getAllFactures = ()=>{
    return makeRequest("http://localhost:8080/invoice", 'GET')
}

export const getProjet = (id)=>{
    return makeRequest(`http://localhost:8080/quote/${id}`, 'GET')
}

export const getFacture = (id)=>{
    return makeRequest(`http://localhost:8080/invoice/${id}`, 'GET')
}

export const transformProjetInFacture  = (id)=>{
    return makeRequest(`http://localhost:8080/quote/transform-to-invoice/${id}`, 'POST')
}

export const createLigne  = (id, body)=>{
    return makeRequest(`http://localhost:8080/quote/add-line/${id}`, 'PUT', body)
}

export const createProjet  = (body)=>{
    return makeRequest(`http://localhost:8080/quote`, 'POST', body)
}