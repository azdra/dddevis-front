ARG NODE_VERSION=20
FROM node:${NODE_VERSION}-alpine AS node_api

WORKDIR /srv/app

RUN npm -g install npm@latest

USER node

EXPOSE ${FRONT_PORT}

CMD ["npm", "run", "dev"]