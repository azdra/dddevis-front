start:
	@docker compose up

stop:
	@docker compose down

install:
	@docker compose run --rm web npm install

enter:
	@docker compose exec web sh